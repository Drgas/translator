# Pstack
Ptranslator jest implementacją translatora gaderypoluki w Pythonie. Zawiera logikę biznesową w formie klasy.
## Technologie
* Python 3.7 i wyżej
* unittest
* pytest
## Uwagi techniczne
### Preferowane IDE
VS Code, PyCharm
### pytest
Przed użyciem biblioteki pytest należy upewnić się co do wcześniejszej instalacji za pomocą modułu pip, np.: `py -m pip install pytest`.
### Uruchamianie testów
* unittest

`py -m unittest tests/unittest_gaderypolukiTest.py`

`py -m unittest tests/unittest_gaderypolukiToolTest.py`

`py -m unittest tests/translatorTestTdd.py`

* pytest

`py -m pytest tests/pytest_gaderypolukiTest.py`

`py -m pytest tests/pytest_gaderypolukiToolTest.py`
